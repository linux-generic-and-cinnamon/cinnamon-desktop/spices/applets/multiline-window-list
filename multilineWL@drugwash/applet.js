// Multi-line taskbar applet for Cinnamon (Linux Mint)
//
// This is a clone of the original Cinnamon applet that adds support
// for a multi-line taskbar.
//
// Author: Dumitru Postoronca (https://github.com/posto)
// Contributors:
//  * jongough (https://github.com/jongough)
//  * Balázs Németh (https://github.com/nbali)
//  * Izzy (https://github.com/IzzySoft)
//  * Drugwash (https://github.com/Drugwash2)
//
// License: GPLv2 (see the COPYING file)
//
//	This program is distributed in the hope that it will be useful,
//	but WITHOUT ANY WARRANTY; without even the implied warranty of
//	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//	GNU General Public License for more details.
//
//	You should have received a copy of the GNU General Public License
//	along with this program.  If not, see <http://www.gnu.org/licenses/>.
//


/**

StBoxLayout panelRight
	StBoxLayout applet-box (js/ui/applet.js, cinnamon internal)
		StBoxLayout windowList
			n x StBin, height: 58px ce contine o aplicatie
				StBin appMenu, height: ~20px
					CinnamonGenericContainer
						StBin 24x19
						CinnamonSlicer 16x16 (icon?)
						StLabel 39x15

global.stage.get_actor_at_pos(Clutter.PickMode.ALL, 427, 1046).get_parent().get_parent().get_children().length

**/

/* CODE, DO NOT CHANGE ANYTHING BELOW THIS LINE */
const Settings = imports.ui.settings;
const Applet = imports.ui.applet;
const Clutter = imports.gi.Clutter;
const Lang = imports.lang;
const Cinnamon = imports.gi.Cinnamon;
const GLib = imports.gi.GLib;
const St = imports.gi.St;
const Main = imports.ui.main;
const Tweener = imports.ui.tweener;
const Panel = imports.ui.panel;
const PopupMenu = imports.ui.popupMenu;
const Meta = imports.gi.Meta;
const SignalManager = imports.misc.signalManager;
const Tooltips = imports.ui.tooltips;
const DND = imports.ui.dnd;
const Util = imports.misc.util;
const Mainloop = imports.mainloop;
const ModalDialog = imports.ui.modalDialog;
const WsBase = (typeof global.workspace_manager === 'undefined') ? global.screen : global.workspace_manager;

var UUID;	// we get these values later on
var IID;	// we get these values later on

var ANIMATE = true;
const ANIMATED_ICON_UPDATE_TIMEOUT = 100; // animation timeout [ms]
const SPINNER_SIZE = 24; // fixed size derived from /usr/share/cinnamon/theme/process-working.svg
var SPINNER_ANIMATION_TIME = 2.5; // [seconds]
var SPINNER_SLOW = 3.5; // animation slowdown factor (* animation timeout)
//const ICON_HEIGHT_FACTOR = .64;
const ICON_HEIGHT_FACTOR = 1.0;
let r;
try { r = global.settings.get_boolean('panels-resizable'); }
catch(e) { r = null;}
const isOld = (typeof r === undefined || r === null) ? false : true;
const allocNS = 300; // [Drugwash] just trying to make things more flexible
const intPadW = 1; // [Drugwash] flexible internal horizontal padding
//const minButW = this.iconSize + 2 * intPadW + 20; // minimum button width
var minButW;
var LWM = true;	// log warning messages
var debug = false;	// log debug messages
const hdr = "MRT: " // logging header
_log("MULTI-ROW APPLET INITIALIZING");
_log("isold=" + isOld.toString());

// =======================================================[ RightClickMenu ]===
function AppMenuButtonRightClickMenu(actor, metaWindow, orientation, applet) {
	this._init(actor, metaWindow, orientation, applet);
}

AppMenuButtonRightClickMenu.prototype = {
	__proto__: PopupMenu.PopupMenu.prototype,

	_init: function(actor, metaWindow, orientation, applet) {
		//take care of menu initialization
		PopupMenu.PopupMenu.prototype._init.call(this, actor, 0.0, orientation, 0);
		Main.uiGroup.add_actor(this.actor);
		//Main.chrome.addActor(this.actor, { visibleInOverview: true,
		//								   affectsStruts: false });
		this.actor.hide();
		this.applet = applet;
		this.window_list = this.applet._windows;
		actor.connect('key-press-event', Lang.bind(this, this._onSourceKeyPress));
		this.connect('open-state-changed', Lang.bind(this, this._onToggled));

		this.metaWindow = metaWindow;

		this.itemCloseWindow = new PopupMenu.PopupMenuItem(_("Close"));
		this.itemCloseWindow.connect('activate', Lang.bind(this, this._onCloseWindowActivate));

		this.itemCloseAllWindows = new PopupMenu.PopupMenuItem(_("Close all"));
		this.itemCloseAllWindows.connect('activate', Lang.bind(this, this._onCloseAllActivate));

		this.itemCloseOtherWindows = new PopupMenu.PopupMenuItem(_("Close others"));
		this.itemCloseOtherWindows.connect('activate', Lang.bind(this, this._onCloseOthersActivate));

		if (metaWindow.minimized)
			this.itemMinimizeWindow = new PopupMenu.PopupMenuItem(_("Restore"));
		else
			this.itemMinimizeWindow = new PopupMenu.PopupMenuItem(_("Minimize"));
		this.itemMinimizeWindow.connect('activate', Lang.bind(this, this._onMinimizeWindowActivate));

		this.itemMaximizeWindow = new PopupMenu.PopupMenuItem(_("Maximize"));
		this.itemMaximizeWindow.connect('activate', Lang.bind(this, this._onMaximizeWindowActivate));

		this.itemMoveToLeftWorkspace = new PopupMenu.PopupMenuItem(_("Move to left workspace"));
		this.itemMoveToLeftWorkspace.connect('activate', Lang.bind(this, this._onMoveToLeftWorkspace));

		this.itemMoveToRightWorkspace = new PopupMenu.PopupMenuItem(_("Move to right workspace"));
		this.itemMoveToRightWorkspace.connect('activate', Lang.bind(this, this._onMoveToRightWorkspace));

		this.itemOnAllWorkspaces = new PopupMenu.PopupMenuItem(_("Visible on all workspaces"));
		this.itemOnAllWorkspaces.connect('activate', Lang.bind(this, this._toggleOnAllWorkspaces));

		this.appletAbout =  new PopupMenu.PopupIconMenuItem(_("About..."), "dialog-information", St.IconType.FULLCOLOR);
		this.appletAbout.connect('activate', Lang.bind(this.applet, this.applet.openAbout));

		this.appletSettings = new PopupMenu.PopupIconMenuItem(_("Configure..."), "system-run", St.IconType.FULLCOLOR);
		this.appletSettings.connect('activate', Lang.bind(this, function() {
			Util.spawnCommandLineAsync("xlet-settings applet " + UUID + " " + IID, null, null);}));
		this.appletReload = new PopupMenu.PopupIconMenuItem(_("Reload applet"), "reload_page", St.IconType.FULLCOLOR);
		let cmnd = `dbus-send --session --dest=org.Cinnamon.LookingGlass --type=method_call ` +
						`/org/Cinnamon/LookingGlass org.Cinnamon.LookingGlass.ReloadExtension ` +
						`string:'${UUID}' string:'APPLET'`;
		this.appletReload.connect('activate', Lang.bind(this, function() {
			Util.spawnCommandLine(cmnd); }));
		this.appletSwitch = new PopupMenu.PopupIconMenuItem(_("Switch window list..."), "cs-applets", St.IconType.FULLCOLOR);
		this.appletSwitch.connect('activate', Lang.bind(this, function() {
			Util.spawnCommandLine(`cinnamon-settings applets`); }));

		if (orientation == St.Side.BOTTOM) {
			this.addMenuItem(this.appletSwitch);
			this.addMenuItem(this.appletReload);
			this.addMenuItem(this.appletSettings);
			this.addMenuItem(this.appletAbout);
			this.addMenuItem(new PopupMenu.PopupSeparatorMenuItem());
			this.addMenuItem(this.itemOnAllWorkspaces);
			this.addMenuItem(this.itemMoveToLeftWorkspace);
			this.addMenuItem(this.itemMoveToRightWorkspace);
			this.addMenuItem(new PopupMenu.PopupSeparatorMenuItem());
			this.addMenuItem(this.itemCloseAllWindows);
			this.addMenuItem(this.itemCloseOtherWindows);
			this.addMenuItem(new PopupMenu.PopupSeparatorMenuItem());
			this.addMenuItem(this.itemMinimizeWindow);
			this.addMenuItem(this.itemMaximizeWindow);
			this.addMenuItem(this.itemCloseWindow);
		}
		else {
			this.addMenuItem(this.itemCloseWindow);
			this.addMenuItem(this.itemMaximizeWindow);
			this.addMenuItem(this.itemMinimizeWindow);
			this.addMenuItem(new PopupMenu.PopupSeparatorMenuItem());
			this.addMenuItem(this.itemCloseOtherWindows);
			this.addMenuItem(this.itemCloseAllWindows);
			this.addMenuItem(new PopupMenu.PopupSeparatorMenuItem());
			this.addMenuItem(this.itemMoveToLeftWorkspace);
			this.addMenuItem(this.itemMoveToRightWorkspace);
			this.addMenuItem(this.itemOnAllWorkspaces);
			this.addMenuItem(new PopupMenu.PopupSeparatorMenuItem());
			this.addMenuItem(this.appletAbout);
			this.addMenuItem(this.appletSettings);
			this.addMenuItem(this.appletReload);
			this.addMenuItem(this.appletSwitch);
		}
	 },

	_onToggled: function(actor, event){
		if (!event)
				return;

		if (this.metaWindow.is_on_all_workspaces()) {
			this.itemOnAllWorkspaces.label.set_text(_("Only on this workspace"));
			this.itemMoveToLeftWorkspace.actor.hide();
			this.itemMoveToRightWorkspace.actor.hide();
		} else {
			this.itemOnAllWorkspaces.label.set_text(_("Visible on all workspaces"));
			if (this.metaWindow.get_workspace().get_neighbor(Meta.MotionDirection.LEFT) != this.metaWindow.get_workspace())
				this.itemMoveToLeftWorkspace.actor.show();
			else
				this.itemMoveToLeftWorkspace.actor.hide();

			if (this.metaWindow.get_workspace().get_neighbor(Meta.MotionDirection.RIGHT) != this.metaWindow.get_workspace())
				this.itemMoveToRightWorkspace.actor.show();
			else
				this.itemMoveToRightWorkspace.actor.hide();
		}
		if (this.metaWindow.get_maximized()) {
			this.itemMaximizeWindow.label.set_text(_("Unmaximize"));
		} else {
			this.itemMaximizeWindow.label.set_text(_("Maximize"));
		}
	},

	_onWindowMinimized: function(actor, event){
	},

	_onCloseWindowActivate: function(actor, event){
		this.metaWindow.delete(global.get_current_time());
		this.destroy();
	},

	_onCloseAllActivate: function(actor, event) {
		let metas = new Array();
		for (let i = 0; i < this.window_list.length; i++) {
			if (this.window_list[i].actor.visible) {
				metas.push(this.window_list[i].metaWindow);
			}
		}
		metas.forEach(Lang.bind(this, function(window) {
			window.delete(global.get_current_time());
			}));
	},

	_onCloseOthersActivate: function(actor, event) {
		let metas = new Array();
		for (let i = 0; i < this.window_list.length; i++) {
			if (this.window_list[i].metaWindow != this.metaWindow && this.window_list[i].actor.visible) {
				metas.push(this.window_list[i].metaWindow);
			}
		}
		metas.forEach(Lang.bind(this, function(window) {
			window.delete(global.get_current_time());
			}));
	},

	_onMinimizeWindowActivate: function(actor, event){
		if (this.metaWindow.minimized) {
			this.metaWindow.unminimize();
			this.metaWindow.activate(global.get_current_time());
		}
		else {
			this.metaWindow.minimize();
		}
	},

	_onMaximizeWindowActivate: function(actor, event){
		if (this.metaWindow.get_maximized()){
			this.metaWindow.unmaximize(Meta.MaximizeFlags.HORIZONTAL | Meta.MaximizeFlags.VERTICAL);
		}else{
			this.metaWindow.maximize(Meta.MaximizeFlags.HORIZONTAL | Meta.MaximizeFlags.VERTICAL);
		}
	},

	_onMoveToLeftWorkspace: function(actor, event){
		let workspace = this.metaWindow.get_workspace().get_neighbor(Meta.MotionDirection.LEFT);
		if (workspace) {
			this.actor.destroy();
			this.metaWindow.change_workspace(workspace);
			Main._checkWorkspaces();
		}
	},

	_onMoveToRightWorkspace: function(actor, event){
		let workspace = this.metaWindow.get_workspace().get_neighbor(Meta.MotionDirection.RIGHT);
		if (workspace) {
			this.actor.destroy();
			this.metaWindow.change_workspace(workspace);
			Main._checkWorkspaces();
		}
	},

	_toggleOnAllWorkspaces: function(actor, event) {
		if (this.metaWindow.is_on_all_workspaces())
			this.metaWindow.unstick();
		else
			this.metaWindow.stick();
	},

	_onSourceKeyPress: function(actor, event) {
		let symbol = event.get_key_symbol();
		if (symbol == Clutter.KEY_space || symbol == Clutter.KEY_Return) {
			this.menu.toggle();
			return true;
		} else if (symbol == Clutter.KEY_Escape && this.menu.isOpen) {
			this.menu.close();
			return true;
		} else if (symbol == Clutter.KEY_Down) {
			if (!this.menu.isOpen)
				this.menu.toggle();
			this.menu.actor.navigate_focus(this.actor, Gtk.DirectionType.DOWN, false);
			return true;
		} else
			return false;
	}

};
// =====================================================[ AppletMenuButton ]===
function AppMenuButton(applet, metaWindow, animation, orientation, panel_height) {
	this._init(applet, metaWindow, animation, orientation, panel_height / 2);
}

AppMenuButton.prototype = {
//	__proto__ : AppMenuButton.prototype,

	_init: function(applet, metaWindow, animation, orientation, panel_height) {

		_log("AppMenuButton(" + applet.toString() + ", height: " + panel_height + ")");
			this.actor = new St.Bin({ style_class: 'window-list-item-box',
								reactive: true,
								can_focus: true,
								x_fill: true,
								y_fill: false,
								track_hover: true });

		if (orientation == St.Side.TOP)
			this.actor.add_style_class_name('window-list-item-box-top');
		else
			this.actor.add_style_class_name('window-list-item-box-bottom');

		this.signals = new SignalManager.SignalManager();
		this.actor._delegate = this;
		this.signals.connect(this.actor, 'button-release-event', Lang.bind(this, this._onButtonRelease));
		this.signals.connect(this.actor, 'button-press-event', Lang.bind(this, this._onButtonPress));

		this.metaWindow = metaWindow;

		this._applet = applet;

		let bin = new St.Bin({ name: 'appMenu' });
		this.actor.set_child(bin);

		this._container = new Cinnamon.GenericContainer();

		bin.set_child(this._container);

		this.signals.connect(this._container, 'get-preferred-width',
				Lang.bind(this, this._getContentPreferredWidth));
		this.signals.connect(this._container, 'get-preferred-height',
				Lang.bind(this, this._getContentPreferredHeight));
		this.signals.connect(this._container, 'allocate', Lang.bind(this, this._contentAllocate));

		this._iconBox = new Cinnamon.Slicer({ name: 'appMenuIcon' });
		this.signals.connect(this._iconBox, 'style-changed',
							  Lang.bind(this, this._onIconBoxStyleChanged));
		this.signals.connect(this._iconBox, 'notify::allocation',
							  Lang.bind(this, this._updateIconBoxClip));
		this._container.add_actor(this._iconBox);
		this._label = new St.Label();
		this._container.add_actor(this._label);

		this._iconBottomClip = 0;
		if (!Main.overview.visible || !Main.expo.visible)
				this._visible = true;
		else this._visible = false;
		if (!this._visible)
			this.actor.hide();
		Main.overview.connect('hiding', Lang.bind(this, function () {
			this.show();
		}));
		Main.overview.connect('showing', Lang.bind(this, function () {
			this.hide();
		}));
		Main.expo.connect('hiding', Lang.bind(this, function () {
			this.show();
		}));
		Main.expo.connect('showing', Lang.bind(this, function () {
			this.hide();
		}));
		this.actor.connect('destroy', Lang.bind(this, this._onDestroy));

		this._updateCaptionId = this.metaWindow.connect('notify::title', Lang.bind(this, function () {
			let title = this.getDisplayTitle();
			this._label.set_text(title);
			if (this._tooltip) this._tooltip.set_text(title);
		}));

//		this._spinner = new Panel.AnimatedIcon('process-working.svg', SPINNER_SIZE);
//		this._spinner = new AnimatedIcon('process-working.svg', SPINNER_SIZE, SPINNER_SLOW); // slow=x*100ms
		this._spinner = new AnimatedIcon('process-working.svg', SPINNER_SIZE);
		if (this._spinner.actor) {
			let lc = this._container.get_last_child();
			this._container.add_actor(this._spinner.actor);
//			try {this._spinner.actor.lower_bottom();} // obsoleted in Clutter 1.10
//			try {this._spinner.actor.set_child_below_sibling(this._spinner.actor, null);}
			try {this._spinner.actor.get_parent.set_child_below_sibling(this._spinner.actor, null);}
			catch(e) {_logWarn("can't lower spinner", LWM);}
		} else _logWarn("error creating spinner icon", LWM);
		let tracker = Cinnamon.WindowTracker.get_default();
		this.app = tracker.get_window_app(this.metaWindow);
		this.set_icon(panel_height);
		let title = this.getDisplayTitle();
		if (metaWindow.minimized)
			this._label.set_text("[" + title + "]");
		else
			this._label.set_text(title);

		if(animation){
			this.startAnimation();
			this.stopAnimation();
		}

		//set up the right click menu
		this._menuManager = new PopupMenu.PopupMenuManager(this);
		this.rightClickMenu = new AppMenuButtonRightClickMenu(this.actor, this.metaWindow, orientation, applet);
		this._menuManager.addMenu(this.rightClickMenu);

		this._tooltip = new Tooltips.PanelItemTooltip(this, title, orientation);

		this._draggable = DND.makeDraggable(this.actor);
		this._draggable.connect('drag-begin', Lang.bind(this, this._onDragBegin));
		this._draggable.connect('drag-cancelled', Lang.bind(this, this._onDragCancelled));
		this._draggable.connect('drag-end', Lang.bind(this, this._onDragEnd));

		this.on_panel_edit_mode_changed();
		global.settings.connect('changed::panel-edit-mode', Lang.bind(this, this.on_panel_edit_mode_changed));
		if (isOld)
		 global.settings.connect('changed::window-list-applet-scroll', Lang.bind(this, this.on_scroll_mode_changed));
		this.window_list = this.actor._delegate._applet._windows;
		this.scroll_connector = null;
		if (isOld)
		  this.on_scroll_mode_changed();
	},

	on_panel_edit_mode_changed: function() {
		this._draggable.inhibit = global.settings.get_boolean("panel-edit-mode");
	},

	on_scroll_mode_changed: function() {
		let scrollable = global.settings.get_boolean("window-list-applet-scroll");
		if (scrollable) {
			this.scroll_connector = this.actor.connect('scroll-event', Lang.bind(this, this._onScrollEvent));
		} else {
			if (this.scroll_connector) {
				this.actor.disconnect(this.scroll_connector);
				this.scroll_connector = null;
			}
		}
	},

	_onScrollEvent: function(actor, event) {
		let direction = event.get_scroll_direction();
		let current;
		let vis_windows = new Array();
		for (let i = 0; i < this.window_list.length; i++) {
			if (this.window_list[i].actor.visible) {
				vis_windows.push(i);
			}
		}
		let num_windows = vis_windows.length;
		for (let i = 0; i < num_windows; i++) {
			if (this.window_list[vis_windows[i]].metaWindow.has_focus()) {
				current = i;
				break;
			}
		}
		let target;
		if (direction == 1) {
			target = ((current - 1) >= 0) ? (current - 1) : (num_windows - 1);
		}
		if (direction == 0) {
			target = ((current + 1) <= num_windows - 1) ? (current + 1) : 0;
		}
		this.window_list[vis_windows[target]].metaWindow.activate(global.get_current_time());
	},

	_onDragBegin: function() {
		this._tooltip.hide();
		this._tooltip.preventShow = true;
	},

	_onDragEnd: function() {
		this._applet.myactorbox._clearDragPlaceholder();
		this._tooltip.preventShow = false;
	},

	_onDragCancelled: function() {
		this._applet.myactorbox._clearDragPlaceholder();
		this._tooltip.preventShow = false;
	},

	getDisplayTitle: function() {
		let title = this.metaWindow.get_title();
		if (!title) title = this.app ? this.app.get_name() : '?';
		return title;
	},

	_onDestroy: function() {
		this.metaWindow.disconnect(this._updateCaptionId);
		this._tooltip.destroy();
		this.rightClickMenu.destroy();
	},

	doFocus: function() {
		if (this.metaWindow.has_focus() && !this.metaWindow.minimized) {
			this.actor.add_style_pseudo_class('focus');
			this.actor.remove_style_class_name("window-list-item-demands-attention");
			this.actor.remove_style_class_name("window-list-item-demands-attention-top");
		}
		else {
			this.actor.remove_style_pseudo_class('focus');
		}
	},

	_onButtonRelease: function(actor, event) {
		this._tooltip.hide();
		if (event.get_button() == 1) {
			if ( this.rightClickMenu.isOpen ) {
				this.rightClickMenu.toggle();
			}
			this._windowHandle(false);
		} else if (event.get_button() == 2) {
			if (this._applet._preferences.task_middleclick_action == 0) {
				this.metaWindow.delete(global.get_current_time());
			} else if (this._applet._preferences.task_middleclick_action == 1) {
				this.app.open_new_window(global.get_current_time());
			}
//		} else if (event.get_button() == 3) {
//			this.rightClickMenu.mouseEvent = event;
//			this.rightClickMenu.toggle();
		}
	},

	_onButtonPress: function(actor, event) {
		this._tooltip.hide();
		if (!this.transient && event.get_button() == 3) {
			this.rightClickMenu.mouseEvent = event;
			this.rightClickMenu.toggle();

//			if (this._hasFocus()) {
			if (this.has_focus()) {
				this.actor.add_style_pseudo_class('focus');
			}
		}
	},

	_windowHandle: function(fromDrag){
		if ( this.metaWindow.has_focus() ) {
			if (fromDrag){
				return;
			}

			this.metaWindow.minimize();
			this.actor.remove_style_pseudo_class('focus');
		}
		else {
			if (this.metaWindow.minimized) {
				this.metaWindow.unminimize();
			}
			this.metaWindow.activate(global.get_current_time());
			this.actor.add_style_pseudo_class('focus');
		}
	},

	handleDragOver: function(source, actor, x, y, time) {
		if (source instanceof AppMenuButton) return DND.DragMotionResult.CONTINUE;

		if (typeof(this._applet.dragEnterTime) == 'undefined') {
			this._applet.dragEnterTime = time;
		} else {
			if (time > (this._applet.dragEnterTime + 3000))
			{
				this._applet.dragEnterTime = time;
			}
		}

		if (time > (this._applet.dragEnterTime + 300)) {
			this._windowHandle(true);
		}
		return DND.DragMotionResult.NO_DROP;
	},

	acceptDrop: function(source, actor, x, y, time) {
		return false;
	},

	show: function() {
		if (this._visible)
			return;
		this._visible = true;
		this.actor.show();
	},

	hide: function() {
		if (!this._visible)
			return;
		this._visible = false;
		this.actor.hide();
	},

	_onIconBoxStyleChanged: function() {
		let node = this._iconBox.get_theme_node();
		this._iconBottomClip = node.get_length('app-icon-bottom-clip');
		this._updateIconBoxClip();
	},

	_updateIconBoxClip: function() {
		let allocation = this._iconBox.allocation;
		if (this._iconBottomClip > 0)
			this._iconBox.set_clip(0, 0,
								allocation.x2 - allocation.x1,
								allocation.y2 - allocation.y1 - this._iconBottomClip);
		else this._iconBox.remove_clip();
	},
// there's a problem with the animated icons: when multiple windows are added simultaneously,
// there is only one icon handle that will respond to the calls - the last one.
// All others are lost, complete with their own timer IDs. This leads to warnings in the logs.
	stopAnimation: function() {
		if (this._spinner.actor) {
			Tweener.addTween(this._spinner.actor,
							{	opacity: 0,
							 	time: SPINNER_ANIMATION_TIME,
//								transition: "easeOutQuad",
								transition: "easeInExpo",
								onCompleteScope: this,
								onComplete: function() {
									this._spinner.actor.opacity = 255;
									this._spinner.actor.hide();
								}
							});
		} else _logWarn("spinner missing on stopAnimation()", LWM);
	},

	startAnimation: function() {
		if (this._spinner.actor) this._spinner.actor.show();
		else _logWarn("spinner missing on startAnimation()", LWM);
	},
// This has to be reviewed very carefully!!!
	_getContentPreferredWidth: function(actor, forHeight, alloc) {
		let [minSize, naturalSize] = this._iconBox.get_preferred_width(forHeight);
		alloc.min_size = minSize; // minimum size just enough for icon if we ever get that many apps going
		alloc.natural_size = naturalSize;
		[minSize, naturalSize] = this._label.get_preferred_width(forHeight);
		alloc.min_size = alloc.min_size + Math.max(0, minSize - Math.floor(alloc.min_size / 2));
		if (alloc.min_size < 0) {
			global.logError("Multi-row applet: negative 'alloc.min_size' value = " + alloc.min_size);
			alloc.min_size = minButW; // allocate a decent width value
		}
//		alloc.natural_size = allocNS;
		alloc.natural_size = Math.min(allocNS, alloc.natural_size+naturalSize);
	},

	_getContentPreferredHeight: function(actor, forWidth, alloc) {
		let [minSize, naturalSize] = this._iconBox.get_preferred_height(forWidth);
		alloc.min_size = minSize;
		alloc.natural_size = naturalSize;
		[minSize, naturalSize] = this._label.get_preferred_height(forWidth);
		if (minSize > alloc.min_size)
			alloc.min_size = minSize;
		if (naturalSize > alloc.natural_size)
			alloc.natural_size = naturalSize;
	},

	_contentAllocate: function(actor, box, flags) {
		let allocWidth = box.x2 - box.x1+1;
		let allocHeight = box.y2 - box.y1+1;
		allocWidth = allocWidth < minButW ? minButW : allocWidth;
		let childBox = new Clutter.ActorBox();
// Allocate space for the icon
		let [minWidth, minHeight, naturalWidth, naturalHeight] = this._iconBox.get_preferred_size();

		let direction = this.actor.get_text_direction();

		let yPadding = 0; //Math.floor(Math.max(0, allocHeight - naturalHeight) / 2);
		childBox.y1 = yPadding;
		childBox.y2 = childBox.y1 + Math.min(naturalHeight, allocHeight);
		if (direction == Clutter.TextDirection.LTR) {
//			childBox.x1 = 3;
			childBox.x1 = intPadW; // could this depend on the currently active theme?
			childBox.x2 = childBox.x1 + Math.min(naturalWidth, allocWidth);
		} else {
			childBox.x1 = Math.max(0, allocWidth - naturalWidth);
			childBox.x2 = allocWidth;
		}
		this._iconBox.allocate(childBox, flags);
// Allocate space for the label
		let iconWidth = this.iconSize;
		let iconSpace = Math.max(this.iconSize, childBox.x2 - childBox.x1); // use IconBox width instead of icon width
		[minWidth, minHeight, naturalWidth, naturalHeight] = this._label.get_preferred_size();
		yPadding = Math.floor(Math.max(0, allocHeight - naturalHeight) / 2);
		childBox.y1 = yPadding;
		childBox.y2 = childBox.y1 + Math.min(naturalHeight, allocHeight);

		if (direction == Clutter.TextDirection.LTR) {
			childBox.x1 = Math.floor(iconSpace + childBox.x1);
			childBox.x2 = Math.min(childBox.x1 + naturalWidth, allocWidth);
		} else {
			childBox.x2 = allocWidth - Math.floor(iconWidth + intPadW); // we'll have to use a constant/variable value here!
			childBox.x1 = Math.max(0, childBox.x2 - naturalWidth);
		}

		this._label.allocate(childBox, flags);

		if (direction == Clutter.TextDirection.LTR) {
			childBox.x1 = Math.floor(iconWidth / 2) + this._label.width;
			childBox.x2 = childBox.x1 + this._spinner.actor.width;
			childBox.y1 = box.y1;
			childBox.y2 = box.y2;
			this._spinner.actor.allocate(childBox, flags);
		} else {
			childBox.x1 = -this._spinner.actor.width;
			childBox.x2 = childBox.x1 + this._spinner.actor.width;
			childBox.y1 = box.y1;
			childBox.y2 = box.y2;
			this._spinner.actor.allocate(childBox, flags);
		}
	},

	getDragActor: function() {
		return new Clutter.Clone({ source: this.actor });
	},

	// Returns the original actor that should align with the actor
	// we show as the item is being dragged.
	getDragActorSource: function() {
		return this.actor;
	},

	set_icon: function(panel_height) {
		if (isOld && global.settings.get_boolean('panel-scale-text-icons')) {
			this.iconSize = Math.round(panel_height * ICON_HEIGHT_FACTOR);
		}
//	  else if (global.settings.get_property('panel-zone-icon-sizes', 'center')) {
	else if (global.settings.get_string('panel-zone-icon-sizes')) {
		let numRows = this._applet._preferences.dynRows ?
			Math.min(this._applet._preferences.taskbar_row_count,
					Math.max(1,
							Math.ceil(this._applet._windows.length /
									this._applet._preferences.min_buttons_per_line) //- 1
					)
			) : this._applet._preferences.taskbar_row_count;
//		this.iconSize = Math.floor((panel_height - numRows + 1) / numRows * ICON_HEIGHT_FACTOR * global.ui_scale);
/*
//This works just like normal row height
		this.iconSize = Math.min(this._applet._preferences.default_icon_size,
			Math.floor((panel_height - 3*numRows + 1) / numRows * ICON_HEIGHT_FACTOR));
*/
		this.iconSize = Math.min(Math.max(this._applet._preferences.default_icon_size,
			Math.floor(panel_height/2)), Math.max(this._applet._preferences.default_icon_size,
			Math.floor((panel_height - 3*numRows + 1) / numRows * ICON_HEIGHT_FACTOR)));
	}
	else this.iconSize = this._applet._preferences.default_icon_size;
//global.log("final icon size=" + this.iconSize.toString());
		let icon = this.app ?
					this.app.create_icon_texture(this.iconSize) :
					new St.Icon({ icon_name: 'application-default-icon',
								 icon_type: St.IconType.FULLCOLOR,
								 icon_size: this.iconSize });
		this._iconBox.set_child(icon);
		minButW = this.iconSize + 2 * intPadW + 20
	}
};
// ========================================================[ The Appletbox ]===
function MultiRowBox(applet) {
	this._init(applet);
}

MultiRowBox.prototype = {
	_init: function(applet) {

		_log("MultiRowBox initializing...");

		this.actor = new St.BoxLayout({ name: 'windowList',
//									   	style_class: 'window-list-box' });
									   	style_class: 'grouped-window-list' });
		this.actor._delegate = this;

		this._applet = applet;

		this._dragPlaceholder = null;
		this._dragPlaceholderPos = -1;
		this._animatingPlaceholdersCount = 0;
	},

	handleDragOver: function(source, actor, x, y, time) {
		if (!(source instanceof AppMenuButton)) return DND.DragMotionResult.NO_DROP;

		let children = this.actor.get_children();
		let windowPos = children.indexOf(source.actor);

		let pos = 0;

		for (var i in children){
			if (x > children[i].get_allocation_box().x1 + children[i].width / 2) pos = i;
		}

		if (pos != this._dragPlaceholderPos) {
			this._dragPlaceholderPos = pos;

			// Don't allow positioning before or after self
			if (windowPos != -1 && pos == windowPos) {
				if (this._dragPlaceholder) {
					this._dragPlaceholder.animateOutAndDestroy();
					this._animatingPlaceholdersCount++;
					this._dragPlaceholder.actor.connect('destroy',
						Lang.bind(this, function() {
							this._animatingPlaceholdersCount--;
						}));
				}
				this._dragPlaceholder = null;

				return DND.DragMotionResult.CONTINUE;
			}

			// If the placeholder already exists, we just move
			// it, but if we are adding it, expand its size in
			// an animation
			let fadeIn;
			if (this._dragPlaceholder) {
				this._dragPlaceholder.actor.destroy();
				fadeIn = false;
			} else {
				fadeIn = true;
			}

			this._dragPlaceholder = new DND.GenericDragPlaceholderItem();
			this._dragPlaceholder.child.set_width (source.actor.width);
			this._dragPlaceholder.child.set_height (source.actor.height);
			this.actor.insert_actor(this._dragPlaceholder.actor,
										this._dragPlaceholderPos);
			if (fadeIn)
				this._dragPlaceholder.animateIn();
		}

		return DND.DragMotionResult.MOVE_DROP;
	},

	acceptDrop: function(source, actor, x, y, time) {
		if (!(source instanceof AppMenuButton)) return false;

		this.actor.move_child(source.actor, this._dragPlaceholderPos);

		this._clearDragPlaceholder();
		actor.destroy();

		return true;
	},

	_clearDragPlaceholder: function() {
		if (this._dragPlaceholder) {
			this._dragPlaceholder.animateOutAndDestroy();
			this._dragPlaceholder = null;
			this._dragPlaceholderPos = -1;
		}
	}
};
// ====================================================[ The Applet itself ]===
function MultiRow(metadata, orientation, panel_height, instanceId) {
	this._init(metadata, orientation, panel_height, instanceId);
}

MultiRow.prototype = {
	__proto__: Applet.Applet.prototype,

	_init: function(metadata, orientation, panel_height, instanceId) {
		_log("MultiRow._init(height: " + panel_height + ")");

		Applet.Applet.prototype._init.call(this, orientation, panel_height);
		this.actor.set_track_hover(false);
		UUID = metadata.uuid; IID = instanceId;

		// bind preferences from settings GUI to our properties
		this._preferences = {};
		this.settings = new Settings.AppletSettings(this._preferences, metadata["uuid"], instanceId);
		this.settings.bind("dynRows","dynRows",this.on_settings_changed);
		this.settings.bind("taskbar_row_count","taskbar_row_count",this.on_settings_changed);
		this.settings.bind("min_buttons_per_line","min_buttons_per_line",this.on_settings_changed);
		this.settings.bind("taskbar_all_workspaces","taskbar_all_workspaces",this.on_settings_changed);
		this.settings.bind("task_middleclick_action","task_middleclick_action",this.on_settings_changed);
		this.settings.bind("default_icon_size","default_icon_size",this.on_settings_changed);
		this.settings.bind("animate","animate",this.on_settings_changed);
		this.settings.bind("spintime","spintime",this.on_settings_changed);
		this.settings.bind("slow_factor","slow_factor",this.on_settings_changed);
		this.settings.bind("log_warnings","log_warnings",this.on_settings_changed);
		this.settings.bind("full_debug","full_debug",this.on_settings_changed);

		ANIMATE = this._preferences.animate;
		SPINNER_ANIMATION_TIME = this._preferences.spintime;
		SPINNER_SLOW = this._preferences.slow_factor;
		LWM = this._preferences.log_warnings;
		debug = this._preferences.full_debug;

		try {
			this.orientation = orientation;
			this.dragInProgress = false;

			// CREATE THE VERTICAL GRID
			let grid = new St.BoxLayout();
			grid.set_vertical(true);

			// CREATE THE WINDOW LIST BOXES
			this.myactorbox = [];
			this.myactor = [];

			_log("TASKBAR_ROW_COUNT=" + this._preferences.taskbar_row_count);
			_log("Actor-array before=" + this.myactor.length);

			for (let i=0; i<this._preferences.taskbar_row_count; i++) {
				_log("Creating taskbar row " + i);
				this.myactorbox.push(new MultiRowBox(this));
				this.myactor.push(this.myactorbox[i].actor); // myactor is St.BoxLayout('windowList')

				if (orientation == St.Side.TOP) {
					this.myactor[i].add_style_class_name('window-list-box-top');
					this.myactor[i].set_style('margin-top: 0px;');
					this.myactor[i].set_style('padding-top: 0px;');
				} else {
					this.myactor[i].add_style_class_name('window-list-box-bottom');
					this.myactor[i].set_style('margin-bottom: 0px;');
					this.myactor[i].set_style('padding-bottom: 0px;');
				}

				// add new row to the grid
				grid.add(this.myactorbox[i].actor);
				_log("Created taskbar row " + i);
			}

			_log("Actor-array after=" + this.myactor.length);

			// this.actor = St.BoxLayout('applet-box'), boxul in care sta applet-ul
			// adaugam in applet-box, pe MultiRowBox=St.BoxLayout('windowList')
			this.actor.add(grid);
			this.actor.reactive = global.settings.get_boolean("panel-edit-mode");

			if (orientation == St.Side.TOP) {
				this.actor.set_style('margin-top: 0px;');
				this.actor.set_style('padding-top: 0px;');
			} else {
				this.actor.set_style('margin-bottom: 0px;');
				this.actor.set_style('padding-bottom: 0px;');
			}

			this._windows = new Array();

			let tracker = Cinnamon.WindowTracker.get_default();
			tracker.connect('notify::focus-app', Lang.bind(this, this._onFocus));

			this.switchWorkspaceHandler = global.window_manager.connect('switch-workspace',
											Lang.bind(this, this._refreshItems));
			global.window_manager.connect('minimize',
											Lang.bind(this, this._onMinimize));
			try { // Cinnamon < 5.4
				global.window_manager.connect('maximize',
											Lang.bind(this, this._onMaximize));
				global.window_manager.connect('unmaximize',
											Lang.bind(this, this._onMaximize));
			} catch(e) { // Cinnamon >= 5.4
				global.window_manager.connect('size-change',
											Lang.bind(this, this._onSizeChange));
			}
			global.window_manager.connect('map',
											Lang.bind(this, this._onMap));

			Main.expo.connect('showing', Lang.bind(this,
							function(){ global.window_manager.disconnect(this.switchWorkspaceHandler);}));
			Main.expo.connect('hidden', Lang.bind(this,
							function(){ this.switchWorkspaceHandler=global.window_manager.connect('switch-workspace',
											Lang.bind(this, this._refreshItems));
										this._refreshItems();}));

			Main.overview.connect('showing', Lang.bind(this,
							function(){ global.window_manager.disconnect(this.switchWorkspaceHandler);}));
			Main.overview.connect('hidden', Lang.bind(this,
							function(){ this.switchWorkspaceHandler=global.window_manager.connect('switch-workspace',
											Lang.bind(this, this._refreshItems));
										this._refreshItems();}));

			this._workspaces = [];
			this._changeWorkspaces();
			WsBase.connect('notify::n-workspaces',
									Lang.bind(this, this._changeWorkspaces));
			global.display.connect('window-demands-attention', Lang.bind(this, this._onWindowDemandsAttention));
			global.display.connect('window-marked-urgent', Lang.bind(this, this._onWindowDemandsAttention));

			// this._container.connect('allocate', Lang.bind(Main.panel, this._allocateBoxes));

			global.settings.connect('changed::panel-edit-mode', Lang.bind(this, this.on_panel_edit_mode_changed));
		}
		catch (e) {
			global.logError(e);
		}
	},

	on_applet_added_to_panel: function() {
		let nWorkspaces = WsBase.get_n_workspaces();
		for ( let wsIdx=0 ; wsIdx < nWorkspaces ; wsIdx++ ) {
			let ws = WsBase.get_workspace_by_index(wsIdx);
			let windows = ws.list_windows();
			for ( let idx=0 ; idx < windows.length ; idx++ ) {
				this._windowAdded(ws, windows[idx]);
			}
		}
//		this._refreshItems();
	},

	on_settings_changed: function() {
	// could not yet figure an automatic action to make the changes visible
	// manual workarounds: see https://github.com/posto/cinnamon-multi-line-taskbar-applet/issues/7#issuecomment-261635031
	// this._refreshItems();
		ANIMATE = this._preferences.animate;
		SPINNER_ANIMATION_TIME = this._preferences.spintime;
		SPINNER_SLOW = this._preferences.slow_factor;
		LWM = this._preferences.log_warnings;
		debug = this._preferences.full_debug;
	},

	on_applet_clicked: function(event) {

	},

	on_panel_edit_mode_changed: function() {
		this.actor.reactive = global.settings.get_boolean("panel-edit-mode");
	},


	_onWindowDemandsAttention : function(display, window) {
		for ( let i=0; i<this._windows.length; ++i ) {
			if ( this._windows[i].metaWindow == window ) {
				this._windows[i].actor.add_style_class_name("window-list-item-demands-attention");
			}
		}
	},

	_onFocus: function() {
		for ( let i = 0; i < this._windows.length; ++i ) {
			let window = this._windows[i];
			window.set_icon(this._panelHeight);
			window.doFocus();
		}
	},

	on_panel_height_changed: function() {
	_log("on_panel_height_changed()");
		this._refreshItems();
	},

	_refreshItems: function() {
		_log("_refreshItems");
		// clean all buttons, for now
		this._clearTaskbarItems();

		for ( let i = 0; i < this._windows.length; ++i ) {
			let metaWindow = this._windows[i].metaWindow;
			if (metaWindow.get_workspace().index() == WsBase.get_active_workspace_index()
					  || metaWindow.is_on_all_workspaces()
					  || this._preferences.taskbar_all_workspaces == 1) {
				this._addWindowBtnToTaskbar(this._windows[i], i);
				this._windows[i].actor.show();
			} else {
				this._windows[i].actor.hide();
			}
		}

		this._onFocus();
	},

	_onWindowStateChange: function(state, actor) {
		for ( let i=0; i<this._windows.length; ++i ) {
			if ( this._windows[i].metaWindow == actor.get_meta_window() ) {
				let windowReference = this._windows[i];
				let menuReference = this._windows[i].rightClickMenu;
				let title = windowReference.getDisplayTitle();

				if (state == 'minimize') {
					windowReference._label.set_text("["+ title +"]");
					menuReference.itemMinimizeWindow.label.set_text(_("Restore"));

					return;
				} else if (state == 'map') {
					windowReference._label.set_text(title);
					menuReference.itemMinimizeWindow.label.set_text(_("Minimize"));

					return;
				}
			}
		}
	},

	_onMinimize: function(cinnamonwm, actor) {
		this._onWindowStateChange('minimize', actor);
	},

	_onMaximize: function(cinnamonwm, actor) {
		this._onWindowStateChange('maximize', actor);
	},

	_onSizeChange: function(cinnamonwm, actor, change) {
// this doesn't seem to exist (yet?)
//		if (change === Meta.SizeChange.MINIMIZE) {
//			this._onMinimize(cinnamonwm, actor);
//			this._onWindowStateChange('minimize', actor);
//		} else if (change === Meta.SizeChange.MAXIMIZE) {
		if (change === Meta.SizeChange.MAXIMIZE) {
//			this._onMaximize(cinnamonwm, actor);
			this._onWindowStateChange('maximize', actor);
		} else if (change === Meta.SizeChange.UNMAXIMIZE) {
//			this._onMaximize(cinnamonwm, actor);
			this._onWindowStateChange('maximize', actor);
		}
	},

	_onMap: function(cinnamonwm, actor) {
		/* Note by Clem: The call to this._refreshItems() below doesn't look necessary.
		 * When a window is mapped in a quick succession of times (for instance if
		 * the user repeatedly minimize/unminimize the window by clicking on the window list,
		 * or more often when the showDesktop button maps a lot of minimized windows in a quick succession..
		 * when this happens, many calls to refreshItems are made and this creates a memory leak.
		 * It also slows down all the mapping and so it takes time for all windows to get unminimized after showDesktop is clicked.
		 *
		 * For now this was removed. If it needs to be put back, this isn't the place.
		 * If showDesktop needs it, then it should only call it once, not once per window.
		 */
		//this._refreshItems();
		this._onWindowStateChange('map', actor);
	},

	_windowAdded: function(metaWorkspace, metaWindow) {
	try {
		_log("_windowAdded(" + metaWindow + ")");
		if (!Main.isInteresting(metaWindow))
			return;
		for ( let i=0; i<this._windows.length; ++i ) {
			if ( this._windows[i].metaWindow == metaWindow ) {
				return;
			}
		}

		//							 (applet, metaWindow, animation, orientation,	panel_height)
		let appbutton = new AppMenuButton(this, metaWindow, ANIMATE, this.orientation, this._panelHeight);
		this._windows.push(appbutton);

		if (this._preferences.taskbar_all_workspaces == 0 &&
			metaWorkspace.index() != WsBase.get_active_workspace_index()) {
			appbutton.actor.hide();
		} else {
			// add actor only if it should be shown
			this._addWindowBtnToTaskbar(appbutton, this._windows.length - 1);
		}
	} catch (e) {
			global.logError(e);
		}

		this._refreshItems();
	},
// This has been plagueing xsession-errors with tons of warnings !
	_clearTaskbarItems: function() {
	_log("_clearTaskbarItems()");
		for ( let i=0; i<this._windows.length; ++i ) {
			for (let act=0; act<this._preferences.taskbar_row_count; act++) {
				if (typeof this._windows[i].actor != "undefined" &&
					this._windows[i].actor.get_parent() == this.myactor[act]) {
					try {this.myactor[act].remove_actor(this._windows[i].actor); }
					catch(e) { _logWarn("Trying to remove nonexistant taskbar item.", LWM); }
				}
			}
		}
	},

	_addWindowBtnToTaskbar: function(appbutton, index) {
		_log("adding button to grid...");

		let idx = 0;
		if (this._windows.length <= this._preferences.min_buttons_per_line * this._preferences.taskbar_row_count) {
			idx = Math.floor(index / this._preferences.min_buttons_per_line);
		} else {
			idx = Math.floor(this._preferences.taskbar_row_count * index / this._windows.length);
		}

		// myactor is an StBoxLayout.windowList -- effectively the taskbar
		// appbutton.actor is St.Bin.window-list-item-box, the element that contains a task in the taskbar
		this.myactor[Math.min(this.myactor.length - 1, idx)].add(appbutton.actor);

		_log("For index=" + idx + " childcount=" + this.myactor[idx].get_children().length);
		_log("adding button to grid...DONE");

	},

	_windowRemoved: function(metaWorkspace, metaWindow) {
		for ( let i=0; i<this._windows.length; ++i ) {
			if ( this._windows[i].metaWindow == metaWindow ) {
					for (let act=0; act<this._preferences.taskbar_row_count; act++) {
						this.myactor[act].remove_actor(this._windows[i].actor);
					}
				this._windows[i].actor.destroy();
				this._windows.splice(i, 1);
				break;
			}
		}

		this._refreshItems();
	},

	_changeWorkspaces: function() {
		for ( let i=0; i<this._workspaces.length; ++i ) {
			let ws = this._workspaces[i];
			ws.disconnect(ws._windowAddedId);
			ws.disconnect(ws._windowRemovedId);
		}

		this._workspaces = [];
		for ( let i=0; i<WsBase.n_workspaces; ++i ) {
			let ws = WsBase.get_workspace_by_index(i);
			this._workspaces[i] = ws;
			ws._windowAddedId = ws.connect('window-added',
									Lang.bind(this, this._windowAdded));
			ws._windowRemovedId = ws.connect('window-removed',
									Lang.bind(this, this._windowRemoved));
		}
	},

	_allocateBoxes: function(container, box, flags) {
		_log("MultiRow._allocateBoxes(" + container + ", " + box + ")");
		let allocWidth = box.x2 - box.x1;
		let allocHeight = box.y2 - box.y1;

		_log("MultiRow._allocateBoxes(allocWidth:  " + allocWidth + ")");
		_log("MultiRow._allocateBoxes(allocHeight: " + allocHeight + ")");


		let [leftMinWidth, leftNaturalWidth] = this._leftBox.get_preferred_width(-1);
		let [centerMinWidth, centerNaturalWidth] = this._centerBox.get_preferred_width(-1);
		let [rightMinWidth, rightNaturalWidth] = this._rightBox.get_preferred_width(-1);

		let sideWidth, centerWidth;
		centerWidth = centerNaturalWidth;
		sideWidth = (allocWidth - centerWidth) / 2;

		let childBox = new Clutter.ActorBox();

		childBox.y1 = 0;
		childBox.y2 = allocHeight;
		if (this.myactor[0].get_text_direction() == Clutter.TextDirection.RTL) {
			childBox.x1 = allocWidth - Math.min(allocWidth - rightNaturalWidth,
												leftNaturalWidth);
			childBox.x2 = allocWidth;
		} else {
			childBox.x1 = 0;
			childBox.x2 = Math.min(allocWidth - rightNaturalWidth, leftNaturalWidth);
		}
		this._leftBox.allocate(childBox, flags);

		childBox.x1 = Math.ceil(sideWidth);
		childBox.y1 = 0;
		childBox.x2 = childBox.x1 + centerWidth;
		childBox.y2 = allocHeight;
		this._centerBox.allocate(childBox, flags);

		childBox.y1 = 0;
		childBox.y2 = allocHeight;
		if (this.myactor[0].get_text_direction() == Clutter.TextDirection.RTL) {
			childBox.x1 = 0;
			childBox.x2 = Math.min(Math.floor(sideWidth),
								   rightNaturalWidth);
		} else {
			childBox.x1 = allocWidth - Math.min(Math.floor(sideWidth),
												rightNaturalWidth);
			childBox.x2 = allocWidth;
		}
		this._rightBox.allocate(childBox, flags);
	}
};
// ========================================[ Duplicate & fix from panel.js ]===
function AnimatedIcon(name, size, slow=1) {
	this._init(name, size, slow);
}

AnimatedIcon.prototype = {
	_init: function(name, size, slow) {
		let factor = (slow <= 0) ? 0.1 : slow;
		this.actor = new St.Bin({ visible: false });
		this.actor.connect('destroy', Lang.bind(this, this._onDestroy));
		this.actor.connect('notify::visible', Lang.bind(this, function() {
			if (this.actor.visible) {
				this._timeoutId = Mainloop.timeout_add(ANIMATED_ICON_UPDATE_TIMEOUT*factor, Lang.bind(this, this._update));
			} else {
				if (this._timeoutId)
//					try { Mainloop.source_remove(this._timeoutId); }catch(e) {}
				this._timeoutId = 0;
			}
		}));

		this._timeoutId = 0;
		this._i = 0;
		this._animations = St.TextureCache.get_default().load_sliced_image(global.datadir + '/theme/' + name, size, size, null);
		this.actor.set_child(this._animations);
	},

	_update: function() {
		this._animations.hide(); // clutter_actor_hide_all() was deprecated in Clutter 1.10
		this._c = this._animations.get_n_children();
		if (this._c == 0 || !this._timeoutId) return false;
		if (this._i > 0)
			this._animations.get_child_at_index(this._i - 1).hide();
		else if (this._i == 0)
			this._animations.get_child_at_index(this._c-1).hide();
		this._animations.get_child_at_index(this._i).show();
		this._i = (this._i < this._c-1) ? this._i + 1 : 0;
		this._animations.show();
		return true;
	},

	_onDestroy: function() {
		if (this._timeoutId) {
//			try { Mainloop.source_remove(this._timeoutId); }
//			catch(e) { global.logWarning("multilineWL.AnimatedIcon._onDestroy(): " + e); }
			this._timeoutId = 0;
		}
	}
};
// =========================================[ Logging (for Debug purposes) ]===
function _log(line) {
	if (debug) global.log(hdr + line);
}

function _logWarn(line, force=false) {
	if (debug || force) global.logWarning(hdr + line);
}

function _logErr(line) {
	global.logError(hdr + line);
}
// =================================================================[ Main ]===
function main(metadata, orientation, panel_height, instanceId) {
	_log("main(height:" + panel_height + ")");
	return new MultiRow(metadata, orientation, panel_height, instanceId);
}
